window.onload=function(){
//顶部导航栏选择
    //jq实现
    //思路：
    //1.获取所有的p标签 和div
    //2.给p标签绑定移入事件
    //3.给当前移入的元素添加类名（active_p和acative_box）并且删除其他标签的(active_p和active_box)
    
    // $(function(){
    //     //mouseenter移入事件
    //     $("#nav .left-nav ul li").mouseenter(function(){
    //         //给当前p标签添加active_p类名
    //         $(this).find("p").addClass("active_p")
    //         //给当前p标签的其他p标签删除active_p 类名
    //         $(this).siblings().find("p").removeClass("active_p")
    //         //显示div给当前div添加active_box
    //         $(this).find("div").addClass("active_box")
    //         //给当前的div的其他div删除active_box 类名
    //         $(this).siblings().find("div").removeClass("active_box")
    //     })  
    //     //mouseleave移出事件
    //     $("#nav .left-nav ul li").mouseleave(function(){
    //         $(this).find("p").removeClass("active_p")
    //         $(this).find("div").removeClass("active_box")
    //     })
    // })

    //js实现
    //思路
    //1.获取所有的li标签
    //2.循环遍历
    //3.绑定移入事件
    //4.给p和div分别添加类名
    //5.给其他的p和div删除类名（排他）
    //6.移除

    //1.获取所有的li标签
    let lis=document.querySelectorAll("#nav .left-nav ul li")
    console.log(lis)
    //2.循环遍历
    for(let i = 0;i<lis.length;i++){
    //3.绑定移入事件   
        lis[i].onmouseenter=function(){
            //5.给其他的p和div删除类名（排他）
            for(let j = 0;j<lis.length;j++){
                lis[j].firstElementChild.classList.remove("active_p")
                lis[j].lastElementChild.classList.remove("active_box")
            }
            //5.给p和div分别添加类名
            this.firstElementChild.classList.add("active_p")
            this.lastElementChild.classList.add("active_box")
        }
    }
    //移出
    for(let i = 0;i<lis.length;i++){
        //给每一个li标签 进行移出事件绑定
        lis[i].onmouseleave=function(){
            lis[i].firstElementChild.classList.remove("active_p")
            lis[i].lastElementChild.classList.remove("active_box")
            //如果是多个元素 firstElementChild改成---lis[i].children[0]
        }
    }


//电子书区域的切换
    //jq实现
    //获取元素
    // $("#book .book_left .book_left_top li").click(function(){
    //     //给当前的添加类名 并删除其他节点
    //     $(this).addClass("active_li").siblings().removeClass("active_li")
    //     //获取索引
    //     let num = $(this).index()
    //     //给当前的添加类名 并删除其他兄弟元素的类名
    //     $("#book .book_left .book_lb_right").eq(num).addClass("active_bookri").siblings().removeClass("active_bookri")
    // })
    //js实现
    //思路
    //1.先获取li标签和下面的div
    let lis1 = document.querySelectorAll("#book .book_left .book_left_top li");
    let divs1 = document.querySelectorAll("#book .book_left .book_lb_right");
    console.log(lis1,divs1);
    //2.循环遍历拿到每一个div
    for(let i = 0;i<lis1.length;i++){
         //3.给每一li标签绑定点击事件
         lis1[i].onclick = function(){
            //删除所有类名  排他
            //5.在点击事件之前对（avtive_li）（ative_bookri）删除
            for(let j=0;j<divs1.length;j++){
                 lis1[j].classList.remove("active_li")
                 divs1[j].classList.remove("active_bookri")
            }
            //给当前添加类名
            //4.在点击事件里面给li标签添加类名（avtive_li）并且给对应的div添加类名（ative_bookri）
            //思考？？？怎么知道对应的div是谁
             lis1[i].classList.add("active_li")
             divs1[i].classList.add("active_bookri")
         }
    }

//图书电子书 右侧列表
    //jq实现
     // 思路
    //1.获取li标签
    //2.给li标签进行移入事件绑定
    //3.给对应的子元素添加类名、
    //4.删除其他子元素的类名
    // $("#book .book_right>ul>li").on("mouseenter",function(){
    //     //给当前的子元素添加类名
    //     $(this).find("h3").addClass("active_h3")
    //     $(this).find("div").addClass("active_img")
    //     $(this).find("h3").slideUp(500)
    //     $(this).find("h3").slideDown(500)
    //     //删除其他子元素的类名
    //     $(this).siblings().find("h3").removeClass("active_h3")
    //     $(this).siblings().find("div").removeClass("active_img")
    // })
    //js实现
    //思路
    //1.获取li标签元素
    //2.循环遍历Li标签
    //3.给每个li标签进行移入事件的绑定
    //4.给对应的li和h3添加类名
    //5.在移入事件里面进行类名删除
    let lis_book = document.querySelectorAll("#book .book_right ul li");
    let h3s = document.querySelectorAll("#book .book_right ul li h3");
    let divs2 = document.querySelectorAll("#book .book_right ul li div");
    console.log(h3s,divs2)

    for(let i = 0;i < lis_book.length;i++){
        lis_book[i].onmouseenter=function(){
            for(let j = 0;j < lis_book.length;j++){
                h3s[j]. classList.remove("active_h3");
                divs2[j].classList.remove("active_img");
            }
            console.log(1111)
            h3s[i].classList.add("active_h3");
            divs2[i].classList.add("active_img") 
        }       
    }


    //思路
    //1.先获取li标签和下面的div
    let lis4 = document.querySelectorAll("#clothing .clothing_left .clothing_left_top li");
    let divs4 = document.querySelectorAll("#clothing .clothing_left .clothing_left_buttom");

    //2.循环遍历拿到每一个div
    for(let i = 0;i<lis4.length;i++){
         //3.给每一li标签绑定点击事件
         lis4[i].onclick = function(){
            //删除所有类名  排他
            //5.在点击事件之前对（avtive_li）（ative_bookri）删除
            for(let j=0;j<lis4.length;j++){
                lis4[j].classList.remove("active_lis")
                divs4[j].classList.remove("active_cloth")
            }
            //给当前添加类名
            //4.在点击事件里面给li标签添加类名（avtive_li）并且给对应的div添加类名（ative_bookri）
            //思考？？？怎么知道对应的div是谁
            lis4[i].classList.add("active_lis")
            divs4[i].classList.add("active_cloth")
         }
    }

    //思路
    //1.先获取li标签和下面的div
    let lis5 = document.querySelectorAll("#outsport .outsport_left .outsport_left_top li");
    let divs5 = document.querySelectorAll("#outsport .outsport_left .outsport_left_buttom");

    //2.循环遍历拿到每一个div
    for(let i = 0;i<lis5.length;i++){
         //3.给每一li标签绑定点击事件
         lis5[i].onclick = function(){
            //删除所有类名  排他
            //5.在点击事件之前对（avtive_li）（ative_bookri）删除
            for(let j=0;j<lis5.length;j++){
                lis5[j].classList.remove("active_liss")
                divs5[j].classList.remove("active_outsport")
            }
            //给当前添加类名
            //4.在点击事件里面给li标签添加类名（avtive_li）并且给对应的div添加类名（ative_bookri）
            //思考？？？怎么知道对应的div是谁
            lis5[i].classList.add("active_liss")
            divs5[i].classList.add("active_outsport")
         }
    }

    //思路
    //1.先获取li标签和下面的div
    let lis6 = document.querySelectorAll("#child .child_left .child_left_top li");
    let divs6 = document.querySelectorAll("#child .child_left .child_left_buttom");

    //2.循环遍历拿到每一个div
    for(let i = 0;i<lis6.length;i++){
         //3.给每一li标签绑定点击事件
         lis6[i].onclick = function(){
            //删除所有类名  排他
            //5.在点击事件之前对（avtive_li）（ative_bookri）删除
            for(let j=0;j<lis6.length;j++){
                lis6[j].classList.remove("active_lisc")
                divs6[j].classList.remove("active_child")
            }
            //给当前添加类名
            //4.在点击事件里面给li标签添加类名（avtive_li）并且给对应的div添加类名（ative_bookri）
            //思考？？？怎么知道对应的div是谁
            lis6[i].classList.add("active_lisc")
            divs6[i].classList.add("active_child")
         }
    }
    
    //回到顶部按钮事件
    var dtop = document.querySelector("#foot .foot_top>div img");
    // 当网页向下滑动 30px 出现"返回顶部" 按钮
    window.onscroll = function() {
        scrollFunction()
    };
    
    function scrollFunction() {
        console.log(121);
        if (document.documentElement.scrollTop >30) {
            dtop.style.display = "block";
        } else {
            dtop.style.display = "none";
        }
    }
    //点击事件，点击回到顶部按钮实现回到顶部功能
    dtop.onclick = function(){
        console.log(document.documentElement.scrollTop);
        document.documentElement.scrollTop=0;
    }
}
